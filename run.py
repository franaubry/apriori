# import the libray that we created
from apriori import *
# read the data
items, transactions = read_csv('example.csv')
# compute the frequent item sets with threshold 0.5
freq_item_sets = apriori(items, transactions, 0.5)
# print the frequent item sets
for size in freq_item_sets:
  print('FREQUENT ITEM SETS OF SIZE', size)
  for item_set in freq_item_sets[size]:
    print(item_set)
