def read_csv(filename):
  import csv
  f = open(filename, 'r')
  transactions = list(csv.reader(f))
  # compute the set of all items apearing in at least one transaction
  items = set()
  for transaction in transactions:
    for item in transaction:
      if len(item) > 0:
        items.add(item)
  # convert the transactions to sets
  return list(items), [ set([x for x in t if len(x) > 0]) for t in transactions ]

def compute_item_supports(items, transactions):
  # initialize the dictionary
  support = { }
  # loop of the items
  for item in items:
    # initialize the support set of the current item
    support[item] = set()
    # loop over the transactions
    for i, transaction in enumerate(transactions):
      if item in transaction:
        # the item belongs to this transaction, its index to the support
        support[item].add(i)
  return support

class ItemSet:

  def __init__(self, items, support, max_index):
    self.items = items
    self.support = support
    self.max_index = max_index

  def __str__(self):
    return 'items: ' + str(self.items) + ', support: ' + str(self.support)

def compute_first(items, transactions, threshold, item_support):
  # initialize item sets of size 1
  fis_size_one = [ ]
  for i, item in enumerate(items):
    # compute the frequent of the singleton item set containing this item
    frequency = len(item_support[item]) / len(transactions)
    if frequency >= threshold:
      # the item set is frequent enough, add it
      fis_size_one.append(ItemSet([item], item_support[item], i))
  return fis_size_one

def compute_from_prev(items, transactions, threshold, item_support, fis_prev):
  # initialize the next size item set
  fis_next = [ ]
  # loop over the frequent item size of the previous size
  for item_set in fis_prev:
    # loop over all items starting from the next index
    for i in range(item_set.max_index + 1, len(items)):
      item = items[i]
      # compute the of the set obtained from adding this item to the current item set
      support = item_set.support.intersection(item_support[item])
      if len(support) / len(transactions) >= threshold:
        # the new item set is frequent enough, add it
        fis_next.append(ItemSet(item_set.items + [item], support, i))
  return fis_next

def apriori(items, transactions, threshold):
  # compute support for singleton sets
  item_support = compute_item_supports(items, transactions)
  # initialize a dictionary that will contain the frequent item sets
  fis = { }
  # initialize the frequent item set of size 1
  fis[1] = compute_first(items, transactions, threshold, item_support)
  size = 2
  # while the current size is not empty, build the next size item sets
  while len(fis[size - 1]) > 0:
    fis[size] = compute_from_prev(items, transactions, threshold, item_support, fis[size - 1])
    size += 1
  return fis
